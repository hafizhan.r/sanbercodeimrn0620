
var Animal = class {
    constructor(name, legs) {
        this._name = name
        this._legs = legs
        
    }
    get name(){
      return this._name;
    }
    set name(x) {
      this._name = x;
    }
  }
  
var sheep = new Animal("shaun");

sheep.name = "shaun";
sheep.legs = "4";
sheep.cold_blooded = "False";
console.log("[No 1]")
console.log(sheep.name) 
console.log(sheep.legs)
console.log(sheep.cold_blooded)

console.log("==============================")


var Animal = class {
    constructor(name, legs) {
        this._name = name
        this._legs = legs
        
        
       
    }
    get name(){
      return this._name;
    }
    set name(x) {
      this._name = x;
    }
    get legs(){
      return this._legs;
    }
    set legs(y) {
      this._legs = y;
    }
  }
  
var sheep = new Animal("shaun");

sheep.name = "shaun";
sheep.legs = "4";
sheep.cold_blooded = "False";
console.log("[No 1]")
console.log(sheep.name) 
console.log(sheep.legs)
console.log(sheep.cold_blooded)

console.log("==============================")

class Frog extends Animal {
  constructor(name) {
    super(name);
    
}

var frog = new Frog("buduk")
frog.jump() = "hop hop" 

frog.name = "kodok";
frog.legs = "4";
frog.cold_blooded = "True";

console.log(frog.name) 
console.log(frog.legs)

class Ape extends Animal {
  constructor(name) {
    super(name);
    
}

var ape = new Ape("kera sakti")
ape.yell() = "Auooo"

ape.name = "sungokong";
ape.legs = "2";
ape.cold_blooded = "False";

console.log(ape.name) 
console.log(ape.legs)
